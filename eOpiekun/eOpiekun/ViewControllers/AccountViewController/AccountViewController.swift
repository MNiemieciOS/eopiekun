//
//  AccountViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 02/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import FirebaseAuth
import SVProgressHUD


class AccountViewController: UIViewController {
    
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userCityLabel: UILabel!
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var userAgeLabel: UILabel!
    @IBOutlet weak var userFriendLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var user: UserModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        logOutButton.titleLabel?.adjustsFontSizeToFitWidth = true
        logOutButton.titleLabel?.numberOfLines = 1
        logOutButton.titleLabel?.minimumScaleFactor = 0.3
        
        navigationItem.title = "DANE OSOBOWE"
        
        scrollView.isHidden = true
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        SVProgressHUD.show()
        let serviceGroup = DispatchGroup()
        let queue = DispatchQueue(label: "com.MNiemiec.eOpiekun", attributes: .concurrent, target: .main)
        
        self.getUserDetails(serviceGroup, queue)
        serviceGroup.notify(queue: DispatchQueue.main, execute: {
            self.scrollView.isHidden = false
            SVProgressHUD.dismiss()
        });
    }
    
    func getUserDetails(_ serviceGroup: DispatchGroup,_ queue: DispatchQueue) {
        serviceGroup.enter()
        queue.async(group: serviceGroup, execute: {
            DataBaseManager.sharedInstnace.get(userDetails: (FIRAuth.auth()?.currentUser?.email)!, completion: { (details, error) in
                if error == nil {
                    if let user = UserModel.deserialize(from: details as? NSDictionary) {
                        self.user = user
                        DataBaseManager.sharedInstnace.currentUser = self.user!
                        self.userNameLabel.text = "\((self.user?.firstName)!) \((self.user?.lastName)!)"
                        self.userCityLabel.text = self.user?.userCity
                        self.userAgeLabel.text = "\((self.user?.userAge)!)"
                        
                        self.getUserImage(serviceGroup, queue)
                        self.gerUserFriend(serviceGroup, queue)
                        
                        
                    }
                    serviceGroup.leave()
                }
            })
        })
    }
    
    func getUserImage(_ serviceGroup: DispatchGroup,_ queue: DispatchQueue) {
        serviceGroup.enter()
        queue.async(group: serviceGroup, execute: {
            DataBaseManager.sharedInstnace.get(userImage: DataBaseManager.sharedInstnace.currentUser.userId, completion: { (userImage, error) in
                serviceGroup.leave()

                if error == nil {
                    self.userImage.image = UIImage(data: userImage!)
                }
            })
            
        })
    }
    
    func gerUserFriend(_ serviceGroup: DispatchGroup,_ queue: DispatchQueue) {
        serviceGroup.enter()
        queue.async(group: serviceGroup, execute: {
            if let userFriend = self.user?.friend  {
                if !userFriend.isEmpty{
                    DataBaseManager.sharedInstnace.get(userDetails: userFriend, completion: { (details, error) in
                        if error == nil {
                            let userFriend = UserModel.deserialize(from: details as? NSDictionary)
                            self.user?.userFriendModel = userFriend!
                            self.userFriendLabel.text = "\((self.user?.userFriendModel?.firstName)!) \((self.user?.userFriendModel?.lastName)!)"
                            serviceGroup.leave()
                        }
                    })
                } else {
                    serviceGroup.leave()
                }
            } else {
                serviceGroup.leave()
            }
            
        })
    }
    
    @IBAction func editPressed(_ sender: UIButton) {
    }
    @IBAction func logOutPressed(_ sender: UIButton) {
        
        do {
            try FIRAuth.auth()?.signOut()
        } catch {
            
        }
        dismiss(animated: true, completion: nil)
    }
}


