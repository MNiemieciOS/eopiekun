//
//  LoginViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 02/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import FirebaseAuth
import SCLAlertView

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        FIRAuth.auth()!.addStateDidChangeListener() { auth, user in
            if user != nil {
                DataBaseManager.sharedInstnace.get(userDetails: (user?.email)!, completion: { (user, error) in
                    if error == nil, let userModel = user {
                        DataBaseManager.sharedInstnace.currentUser = userModel
                        self.performSegue(withIdentifier: "LoginUser", sender: nil)
                    }
                })
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let registrationView = segue.destination as? RegistrationViewController {
            registrationView.delegate = self
        }
    }
    @IBAction func LoginButtonPressed(_ sender: UIButton) {
        if !(emailTextField.text?.isEmpty)! {
            if !(passwordTextField.text?.isEmpty)! {
                FIRAuth.auth()!.signIn(withEmail: emailTextField.text!, password: passwordTextField.text!, completion: { (user, error) in
                    if error != nil {
                        SCLAlertView().showError("Error", subTitle: (error?.localizedDescription)!)
                    }
                })
                
            } else {
                passwordTextField.becomeFirstResponder()
            }
        } else {
            emailTextField.becomeFirstResponder()
        }
    }
    @IBAction func registerButtonTapped(_ sender: UIButton) {
    performSegue(withIdentifier: "ShowRegistrationView", sender: nil)
    }
}

extension LoginViewController : UserRegistrationDelegate {
    func userCreated(_ userModel: UserModel) {
        emailTextField.text = userModel.userId
        passwordTextField.text = userModel.password
    }
    
    
}
