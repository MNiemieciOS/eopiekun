//
//  ContactTableViewCell.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 03/04/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var nameLabel: UILabel!

    func configure(withUserId userId: String) {
        DataBaseManager.sharedInstnace.get(userImage: userId) { (userImage, error) in
            if error == nil {
                self.userImage.image = UIImage(data: userImage!)
            }
        }
        DataBaseManager.sharedInstnace.get(userDetails: userId, completion: { (details, error) in
            if error == nil {
                let userDetails = UserModel.deserialize(from: details as? NSDictionary)
                self.nameLabel.text = "\((userDetails?.firstName)!) \((userDetails?.lastName)!)"
            }
        })
    }
}
