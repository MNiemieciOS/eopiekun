//
//  MessageTableViewCell.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 05/04/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var senderImageView: RoundedImageView!
    var messageModel = MessageModel()
    override func awakeFromNib() {
        super.awakeFromNib()
        messageView.layer.borderWidth = 5
        messageView.layer.cornerRadius = 10
        messageView.layer.borderColor = UIColor(red: 12/255.0, green: 180/255.0, blue: 255/255.0, alpha: 1.0).cgColor    }
    
    func configure(withMessage message: MessageModel) {
        messageModel = message
        messageText.text = message.messageDetails
        
        DataBaseManager.sharedInstnace.get(userImage: messageModel.source, completion: { (userImage, error) in
            if error == nil {
                self.senderImageView.image = UIImage(data: userImage!)
            }
        })

    }
}
