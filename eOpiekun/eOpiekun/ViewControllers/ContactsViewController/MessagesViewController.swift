//
//  MessagesViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 05/04/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController {
    
    @IBOutlet weak var newMessageStack: UIStackView!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    var conversation = ConversationModel()
    var isReadMode = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        loadMessages()
        newMessageStack.isHidden = isReadMode
        
        
    }
    
    func loadMessages() {
        DataBaseManager.sharedInstnace.getMessages(forConversation: conversation.conversationId, completion: { (details, error) in
            
            self.conversation.messages = (details as! NSDictionary).allKeys.map({ (key) -> MessageModel in
                let messageModel = (details as! NSDictionary)[key]
                return MessageModel.deserialize(from: messageModel as? NSDictionary)!
            }).sorted(by: { (model1, model2) -> Bool in
                return model1.timestamp < model2.timestamp
            })
            self.tableView.reloadData()
            self.tableView.scrollToBottom()
        })
        
    }
    @IBAction func sendMessagePressed(_ sender: Any) {
        
        let message = MessageModel(withMessageDetails: messageTextView.text)
        
        DataBaseManager.sharedInstnace.sendMessage(forConversation: conversation.conversationId, message: message.toJSON()!) { (refernce, error) in
            if error == nil {
                self.messageTextView.resignFirstResponder()
                self.loadMessages()
            }
        }
    }
}

extension MessagesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversation.messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellId = "MessageHostCell"
        let messageObject = conversation.messages[indexPath.row]
        if messageObject.source != DataBaseManager.sharedInstnace.currentUser.userId {
            cellId = "MessageGuestCell"
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MessageTableViewCell
        cell.configure(withMessage: messageObject)
        return cell
    }
}

extension MessagesViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.white {
            textView.text = nil
            textView.textColor = UIColor.white
            sendMessageButton.isEnabled = true
        }
    }
}

extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0){
            
            self.scrollToRow(at: IndexPath(row: rows - 1, section: sections - 1), at: .bottom, animated: true)
        }
    }
}
