//
//  ContactsViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 06/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var contacts = [String]()
    var destination = ""
    var currentConversation = ConversationModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "ZNAJOMI"
        contacts = DataBaseManager.sharedInstnace.currentUser.contacts
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowConversation" {
            let messagesVC = segue.destination as! MessagesViewController
            messagesVC.conversation = currentConversation
        }
    }
}

extension ContactsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell") as! ContactTableViewCell
        cell.configure(withUserId: contacts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
}

extension ContactsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        destination = contacts[indexPath.row]
        
        
        
        DataBaseManager.sharedInstnace.getConverstions(forUser: DataBaseManager.sharedInstnace.currentUser.userId) { (conversation, error) in
            if error == nil {
                
                let conversationsArray = (conversation as! NSDictionary).allKeys.map({ (key) -> ConversationModel in
                    let convsersationModel = (conversation as! NSDictionary)[key]
                    return ConversationModel.deserialize(from: convsersationModel as? NSDictionary)!
                })
                
                self.currentConversation = conversationsArray.first(where: { (model) -> Bool in
                      return model.destination == self.destination || model.source == self.destination
                })!
                self.performSegue(withIdentifier: "ShowConversation", sender: nil)

                
//                for conversation in conversationsArray {
//                    DataBaseManager.sharedInstnace.getMessages(forConversation: conversation.conversationId, completion: { (details, error) in
//                        
//                        conversation.messages = (details as! NSDictionary).allKeys.map({ (key) -> MessageModel in
//                            let messageModel = (details as! NSDictionary)[key]
//                            return MessageModel.deserialize(from: messageModel as? NSDictionary)!
//                        })
//                        
//                        print("dupa")
//                    })
//                }
            }
        }
        
        
    }
}
