//
//  HealthStatusViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 06/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class HealthStatusViewController: UIViewController {
    
    @IBOutlet weak var pulseView: RoundedUIView!
    @IBOutlet weak var bloodPresureView: RoundedUIView!
    @IBOutlet weak var numberOfStepsView: RoundedUIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pulseView.layer.borderWidth = 10
        pulseView.layer.borderColor = UIColor(red: 255/255.0, green: 219/255.0, blue: 0/255.0, alpha: 1.0).cgColor
    
        bloodPresureView.layer.borderWidth = 10
        bloodPresureView.layer.borderColor = UIColor(red: 32/255.0, green: 191/255.0, blue: 85/255.0, alpha: 1.0).cgColor
        
        numberOfStepsView.layer.borderWidth = 10
        numberOfStepsView.layer.borderColor = UIColor(red: 242/255.0, green: 61/255.0, blue: 58/255.0, alpha: 1.0).cgColor
        
        navigationItem.title = "POMIAR PARAMETRÓW ŻYCIOWYCH"
    }
    
}
