//
//  CasebookTableViewCell.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 29/03/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class CasebookTableViewCell: UITableViewCell {
    
    @IBOutlet weak var diseaseClassificationLabel: UILabel!
    @IBOutlet weak var treatmentStartLabel: UILabel!
    @IBOutlet weak var treatmentEndLabel: UILabel!
    var casebookModel = CasebookModel()
    
    func configure(withCasebook casebook: CasebookModel) {
        casebookModel = casebook
        diseaseClassificationLabel.text = casebookModel.diseaseClassificationId
        treatmentStartLabel.text = casebookModel.treatmentStart
        treatmentEndLabel.text = casebookModel.treatmentEnd
    }
}
