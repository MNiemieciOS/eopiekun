//
//  CasebookViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 02/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import FirebaseAuth
import SVProgressHUD

class CasebookViewController: UIViewController {
    
    var casebooksArray = [CasebookModel]()
    var casebookToShow = CasebookModel()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "HISTORIA CHOROBY"
        
        SVProgressHUD.show()
        DataBaseManager.sharedInstnace.getCasebook(forUser: (FIRAuth.auth()?.currentUser?.email)!) { (details, error) in
            if error == nil && details != nil {
                SVProgressHUD.dismiss()
                
                if let detailsDictionary = details as? NSDictionary {
                    for key in detailsDictionary.allKeys {
                        let casebook = CasebookModel.deserialize(from:detailsDictionary[key] as? NSDictionary)
                        casebook?.casebookId = key as! String
                        self.casebooksArray.append(casebook!)
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            let detailVc = segue.destination as! CasebookDetailsViewController
            detailVc.casebook = casebookToShow
        }
    }
}


extension CasebookViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! CasebookTableViewCell
        cell.configure(withCasebook: casebooksArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return casebooksArray.count
    }
}

extension CasebookViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        casebookToShow = casebooksArray[indexPath.row]
        performSegue(withIdentifier: "showDetails", sender: nil)
    }
}
