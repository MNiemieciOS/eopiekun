//
//  CasebookDetailsViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 29/03/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import SVProgressHUD

class CasebookDetailsViewController: UIViewController {
    var casebook = CasebookModel()
    var commentsArray = [CommentModel]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = casebook.diseaseClassificationId
        SVProgressHUD.show()
        DataBaseManager.sharedInstnace.getCasebookDetails(forCasebookId: casebook.casebookId) { (details, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                let detailsDictionary = details as? NSDictionary
                for key in (detailsDictionary?.allKeys)! {
                    let comment = CommentModel.deserialize(from:detailsDictionary?[key] as? NSDictionary)
                    self.commentsArray.append(comment!)
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension CasebookDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! CommentTableViewCell
        cell.configure(withComment: commentsArray[indexPath.row])
        return cell
    }
}
