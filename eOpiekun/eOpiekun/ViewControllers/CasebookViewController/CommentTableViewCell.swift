//
//  CommentTableViewCell.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 30/03/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    func configure(withComment comment: CommentModel) {
        dateLabel.text = comment.date
        detailsLabel.text = comment.details
    }
}
