//
//  ListOfUsersViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 09/10/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
protocol SelectUserDelegate {
    func selectUser(_ userModel: UserModel)
}

class ListOfUsersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var delegate : SelectUserDelegate?
    let tableViewDataSource = ListOfUserDataProvider()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewDataSource.configure(withUser: "") {
            self.tableView.dataSource = self.tableViewDataSource
            self.tableView.reloadData()
        }
    }
}
extension ListOfUsersViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectUser(tableViewDataSource.getUser(forRow: indexPath.row))
        navigationController?.popViewController(animated: true)
    }
}
