//
//  FriendViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 09/10/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
protocol AddFriendDelegate {
    func addFriend(_ friend: UserModel)
}

class FriendViewController: UIViewController {
    var selectedUser = UserModel()
    var delegate : AddFriendDelegate?
    
    @IBOutlet weak var userIdTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let usersList = segue.destination as? ListOfUsersViewController {
            usersList.delegate = self
        }
    }
   
    @IBAction func addUserTapped(_ sender: UIButton) {
        delegate?.addFriend(selectedUser)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showUsersListTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowUsersList", sender: nil)
    }
}

extension FriendViewController : SelectUserDelegate {
    func selectUser(_ userModel: UserModel) {
        selectedUser = userModel
        userIdTextField.text = selectedUser.userId

    }
}
