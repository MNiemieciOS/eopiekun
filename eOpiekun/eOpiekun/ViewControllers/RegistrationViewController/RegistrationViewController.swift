//
//  RegistrationViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 27/09/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import FirebaseAuth
import SVProgressHUD
import SCLAlertView

protocol UserRegistrationDelegate {
    func userCreated(_ userModel : UserModel)
}

class RegistrationViewController: UIViewController {
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var surenameTextField: UITextField!
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var ageTextField: UITextField!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet weak var userImageView: RoundedImageView!
    
    var userModel = UserModel()
    private var textFieldsArray : [UITextField]?
    var delegate : UserRegistrationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldsArray = [emailTextField, passwordTextField, nameTextField, surenameTextField, cityTextField, ageTextField]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let imagePicker = segue.destination as? ImagePickerViewController {
            imagePicker.delegate = self
        }
        
        if let addUser = segue.destination as? FriendViewController {
            addUser.delegate = self
        }
    }
    
    func prepareModel() -> Void {
        userModel.firstName = nameTextField.text!
        userModel.lastName = surenameTextField.text!
        userModel.userCity = cityTextField.text!
        userModel.userAge = Int(ageTextField.text!)!
        userModel.doctorId = ""
        userModel.userId = emailTextField.text!
        userModel.password = passwordTextField.text!
    }
    
    func checkIfRegistrationIsAllowed() -> Bool {
        textFieldsArray?.forEach({ (textField) in
            if let empty = textField.text?.isEmpty {
                if empty {
                    textField.layer.borderColor = UIColor.red.cgColor
                    textField.layer.borderWidth = 1
                } else {
                    textField.layer.borderColor = UIColor.clear.cgColor
                    textField.layer.borderWidth = 0
                }
            }
        })
        var result = false
        if let allowToRegister = textFieldsArray?.filter({ (textField) -> Bool in
            !(textField.text?.isEmpty)!
        }) {
            result = allowToRegister.count == textFieldsArray?.count
        }
        return result
    }
    
    @IBAction func addFriendTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowAddUser", sender: nil)
    }
    
    @IBAction func registerUserTapped(sender: UIButton) {
        
        if checkIfRegistrationIsAllowed() {
            prepareModel()
                SVProgressHUD.show()
                //TODO Refactor
                DataBaseManager.sharedInstnace.createUser(userModel) { [unowned self](details, error) in
                    FIRAuth.auth()!.createUser(withEmail: self.userModel.userId, password: self.userModel.password, completion: { (user, error) in
                        if error == nil {
                            FIRAuth.auth()?.signIn(withEmail: self.userModel.userId, password: self.userModel.password, completion: { (user, error) in
                                if error == nil {
                                    self.createUserImage(self.userModel.firstName,completion: {(result) in
                                        SVProgressHUD.dismiss(completion: {
                                            self.delegate?.userCreated(self.userModel)
                                            self.navigationController?.popViewController(animated: true)
                                        })
                                    })
                                } else {
                                    SVProgressHUD.dismiss(completion: {
                                        SCLAlertView().showError("Error", subTitle: (error?.localizedDescription)!)
                                    })
                                }
                            })
                        } else {
                            SVProgressHUD.dismiss(completion: {
                                SCLAlertView().showError("Error", subTitle: (error?.localizedDescription)!)
                            })
                        }
                    })
                }
            } else {
                SCLAlertView().showError("Error", subTitle: "All fields are required")
            }
        
        
    }
    
    func createUserImage(_ name: String, completion : @escaping ((Bool) -> Void)) {
        if let imageData = UIImagePNGRepresentation(userImageView.image!) {
            DataBaseManager.sharedInstnace.createUserImage(imageData, user: DataBaseManager.sharedInstnace.currentUser.userId,userImageName: name, completion: { (response, error) in
                completion(error != nil)
            })
        }
    }
    
    @IBAction func selectUserPhotoTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowImagePicker", sender: nil)
    }
}
extension RegistrationViewController : AddFriendDelegate {
    func addFriend(_ friend: UserModel) {
        userModel.userFriendModel = friend
        userModel.friend = friend.userId
    }
}

extension RegistrationViewController : SelectImageDelegate {
    func showUserImage(_ image: UIImage) {
        userImageView.image = image
    }
}
