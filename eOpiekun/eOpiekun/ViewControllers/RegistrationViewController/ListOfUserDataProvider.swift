//
//  ListOfUserDataProvider.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 09/10/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class ListOfUserDataProvider: NSObject {
    var lisfOfUser = [UserModel]()
    func configure(withUser user:String, completion : @escaping (() -> Void)) {
        DataBaseManager.sharedInstnace.getFriendsFor(userDetails: user) {[unowned self] (friends, error) in
            if let response = friends {
                self.lisfOfUser = response
                completion()
            }
        }
    }
    
    func getUser(forRow row: Int) -> UserModel {
        return lisfOfUser[row]
    }
}

extension ListOfUserDataProvider : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lisfOfUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "friendCell")
        let user = lisfOfUser[indexPath.row]
        cell?.textLabel?.text = user.userId
        cell?.detailTextLabel?.text = "\(user.firstName) \(user.lastName)"
        return cell!
    }
}
