//
//  FamilyDoctorViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 08/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import SVProgressHUD

class FamilyDoctorViewController: UIViewController {
    var doctorModel = DoctorModel()
    
    @IBOutlet weak var doctorImageView: RoundedImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var workingPlaceLabel: UILabel!
    @IBOutlet weak var doctorNameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "LEKARZ PIERWSZEGO KONTAKTU"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.show()
        let serviceGroup = DispatchGroup()
        let queue = DispatchQueue(label: "com.MNiemiec.eOpiekun", attributes: .concurrent, target: .main)
        
        self.getDoctorDetails(serviceGroup, queue)
        serviceGroup.notify(queue: DispatchQueue.main, execute: {
            SVProgressHUD.dismiss()
        });
    }
    
    func getDoctorDetails(_ serviceGroup: DispatchGroup,_ queue: DispatchQueue) {
        serviceGroup.enter()
        queue.async(group: serviceGroup, execute: {
            
            DataBaseManager.sharedInstnace.get(doctorDetails: DataBaseManager.sharedInstnace.currentUser.doctorId) { (details, error) in
                if error == nil {
                    if let doctorModel = details as? NSDictionary {
                        self.doctorModel = DoctorModel.deserialize(from: doctorModel)!
                        self.fillDoctorInfo()
                        self.getDoctorImage(serviceGroup, queue)
                    }
                    serviceGroup.leave()
                    
                }
            }
        })
    }
    
    func fillDoctorInfo() {
        doctorNameLabel.text = "\(doctorModel.firstName) \(doctorModel.lastName)"
        workingPlaceLabel.text = doctorModel.workingPlace
        phoneNumberLabel.text = doctorModel.phoneNumber
    }
    
    func getDoctorImage(_ serviceGroup: DispatchGroup,_ queue: DispatchQueue) {
        serviceGroup.enter()
        queue.async(group: serviceGroup, execute: {
            DataBaseManager.sharedInstnace.get(doctorImage: self.doctorModel.doctorId, completion: { (userImage, error) in
                if error == nil {
                    self.doctorImageView.image = UIImage(data: userImage!)
                    serviceGroup.leave()
                }
            })
        })
    }
    
}
