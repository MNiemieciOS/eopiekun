//
//  ImagePickerViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 04/10/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

protocol SelectImageDelegate {
    func showUserImage(_ image: UIImage)
}

class ImagePickerViewController: UIViewController {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var missingImageLabel: UILabel!
    var delegate : SelectImageDelegate?
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        if userImage.image == nil {
            userImage.isHidden = true
        }
    }
    
    @IBAction func takeImageTapped(_ sender: UIButton) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func chooseImageTapped(_ sender: UIButton) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
}

extension ImagePickerViewController : UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            userImage.contentMode = .scaleAspectFit
            userImage.image = pickedImage
            userImage.isHidden = false
            missingImageLabel.isHidden = true
            delegate?.showUserImage(pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension ImagePickerViewController : UINavigationControllerDelegate {
    
}
