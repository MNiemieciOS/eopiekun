//
//  DrawViewController.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 16/10/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import TouchDraw

class DrawViewController: UIViewController {
    
    var currentLeter = 65
    @IBOutlet weak var currentLetterLabel: UILabel!
    @IBOutlet var drawView: TouchDrawView!
    fileprivate func setCurrentLetterLabel() {
        currentLetterLabel.text = String(Character(UnicodeScalar(currentLeter)!))
    }
    @IBOutlet weak var strokeSwitcher: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCurrentLetterLabel()
    }
    
    @IBAction func clear(_ sender: AnyObject) {
        let userLetter = drawView.exportDrawing()
        if let imageData = UIImagePNGRepresentation(userLetter) {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd|MM|YYYY HH:mm:ss"
            let date = formatter.string(from: Date())
            let stroke = strokeSwitcher.isOn ? "niedokrwienny" : "krwotoczny"
            let userInput = "\(currentLetterLabel.text!)/\(stroke)/\(date)"
            DataBaseManager.sharedInstnace.createUserInput(imageData, user: DataBaseManager.sharedInstnace.currentUser.userId, userInput:userInput, completion: {[weak self] (response, error) in
                
                if (self?.currentLeter)! < 69 {
                    self?.currentLeter += 1
                    self?.setCurrentLetterLabel()
                    self?.drawView.clearDrawing()
                } else {
                    self?.currentLeter = 65
                    self?.drawView.clearDrawing()
                }
                
            })
            
        }
    }
}
