//
//  DataBaseManager.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 15/03/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage
import HandyJSON

class DataBaseManager: NSObject {
    
    static let sharedInstnace = DataBaseManager()
    var currentUser = UserModel()
    let rootRef = FIRDatabase.database().reference()
    
    // MARK: - User Details
    func get(userDetails user: String, completion:@escaping ((UserModel?,Error?)-> Void)) {
        let userLogin = user.components(separatedBy: "@").first!
        rootRef.child("Users").child(userLogin).observeSingleEvent(of: .value, with: { (snapshot) in
              let user = UserModel.deserialize(from: snapshot.value as? NSDictionary)!
            completion (user, nil)
        }) { (error) in
            completion (nil, error)
        }
    }
    
    func get(userImage user:String, completion:@escaping ((Data?,Error?)-> Void)) {
//        let userLogin = user.components(separatedBy: "@").first!
        let imageRef = FIRStorage.storage().reference(withPath: "UsersImage/\(user)/profileImage.png")
        imageRef.data(withMaxSize: 1 * 1024 * 1024) { (data, error) in
            completion(data, error)
        }
    }
    
    func createUser(_ userModel: UserModel, completion:@escaping ((Any?,Error?)-> Void)) {
        let userLogin = userModel.userId.components(separatedBy: "@").first!
        
        let newUser = "Users/\(userLogin)"
        rootRef.child(newUser).setValue(userModel.toJSON(), andPriority: nil) { (error, reference) in
            completion (reference, error)
        }
    }
    
    func createUserImage(_ imageData: Data, user:String, userImageName : String, completion: @escaping ((Any?, Error?)-> Void)) {
        let storage = FIRStorage.storage().reference()
//        let userLogin = userId.components(separatedBy: "@").first!

        let userImageRef = storage.child("UsersImage/\(user)/profileImage.png")
        
        userImageRef.put(imageData, metadata: nil) { (metadata, error) in
            if let metadata = metadata {
                completion(metadata,nil)
            } else {
                completion (nil, error)
            }
        }
    }
    
    func createUserInput(_ imageData: Data, user:String, userInput : String, completion: @escaping ((Any?, Error?)-> Void)) {
        let storage = FIRStorage.storage().reference()
        let userImageRef = storage.child("UsersImage/\(user)/\(userInput).png")
        
        userImageRef.put(imageData, metadata: nil) { (metadata, error) in
            if let metadata = metadata {
                completion(metadata,nil)
            } else {
                completion (nil, error)
            }
        }
    }
    
    // MARK: - Casebook Details
    func getCasebook(forUser user:String, completion:@escaping ((Any?,Error?)-> Void)) {
        let userLogin = user.components(separatedBy: "@").first!
        
        rootRef.child("Casebooks").queryOrdered(byChild: "userId").queryEqual(toValue: userLogin).observeSingleEvent(of: .value, with: { (snapshot) in
            completion (snapshot.value, nil)
        }) { (error) in
            completion (nil, error)
        }
    }
    
    func getCasebookDetails(forCasebookId casebookId: String , completion:@escaping ((Any?,Error?)-> Void)) {
        
        rootRef.child("Casebooks/\(casebookId)/comments").observeSingleEvent(of: .value, with: { (snapshot) in
            completion (snapshot.value, nil)
        }) { (error) in
            completion (nil, error)
        }
    }
    
    
    // MARK: - Doctor Details
    func get(doctorDetails doctor:String, completion:@escaping ((Any?,Error?)-> Void)) {
        rootRef.child("Doctors/\(doctor)").observeSingleEvent(of: .value, with: { (snapshot) in
            completion (snapshot.value, nil)
        }) { (error) in
            completion (nil, error)
        }
    }
    
    func get(doctorImage doctor:String, completion:@escaping ((Data?,Error?)-> Void)) {
        let imageRef = FIRStorage.storage().reference(withPath: "DoctorsImage/\(doctor).png")
        imageRef.data(withMaxSize: 1 * 1024 * 1024) { (data, error) in
            completion(data, error)
        }
    }
    
    // MARK: - Conversation
    func getConverstions(forUser userId: String, completion:@escaping ((Any?,Error?)-> Void)) {
        rootRef.child("Conversations").queryOrdered(byChild: "destination").queryEqual(toValue: userId).observeSingleEvent(of: .value, with: { (snapshot) in
            completion (snapshot.value, nil)
        }) { (error) in
            completion (nil, error)
        }
    }
    
    func getMessages(forConversation converstionId: String, completion:@escaping ((Any?,Error?)-> Void)) {
        
        let child = "Conversations/\(converstionId)/messages"
        rootRef.child(child).observeSingleEvent(of: .value, with: { (snapshot) in
            completion (snapshot.value, nil)
        }) { (error) in
            completion (nil, error)
        }
    }
    
    func sendMessage(forConversation converstionId: String, message: [String:Any], completion:@escaping ((Any?,Error?)-> Void)) {
        let messageId = message["messageId"]!
        let child = "Conversations/\(converstionId)/messages/\(messageId))"
        
        rootRef.child(child).setValue(message, andPriority: nil) { (error, reference) in
            completion (reference, error)
        }
    }
    // MARK: - Friends
    
    func getFriendsFor(userDetails userId: String, completion:@escaping (([UserModel]?,Error?)-> Void)) {
        rootRef.child("Users").observeSingleEvent(of: .value, with: { (snapshot) in
            if let response = snapshot.value as? [String : Any]  {
                let userFriends = response.map({ (result) -> UserModel in
                    return UserModel.deserialize(from: result.value as? NSDictionary)!
                }).filter({ (user) -> Bool in
                    return user.userId != userId
                })
                completion (userFriends, nil)
            } else {
                completion (nil, nil)
            }
        }) { (error) in
            completion (nil, error)
        }
    }
    
}
