//
//  DoctorModel.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 30/03/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import HandyJSON

class DoctorModel: HandyJSON {
    var firstName = ""
    var lastName = ""
    var phoneNumber = ""
    var workingPlace = ""
    var doctorId = ""
    var patients = [String]()
    
    required init() {}
}
