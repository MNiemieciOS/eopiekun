//
//  CommentModel.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 29/03/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import Foundation
import HandyJSON

class CommentModel : HandyJSON {
    var date = ""
    var details = ""
    
    required init() {}

}
