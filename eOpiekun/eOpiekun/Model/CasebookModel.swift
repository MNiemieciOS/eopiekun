//
//  CasebookModel.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 29/03/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import Foundation
import HandyJSON

class CasebookModel: HandyJSON {
    var diseaseClassificationId = ""
    var treatmentEnd = ""
    var treatmentStart = ""
    var userId = ""
    var casebookId = ""
    
    required init() {}
}
