//
//  ConversationModel.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 04/04/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import HandyJSON

class ConversationModel: HandyJSON {
    var conversationId = ""
    var destination = ""
    var source = ""
    var timestamp = ""
    var messages = [MessageModel]()
    required init() {}
    
    init(withDestination destinationUser : String) {
        destination = destinationUser
        source = DataBaseManager.sharedInstnace.currentUser.userId
        timestamp = Date().description
        conversationId = "\(timestamp)_\(source)"
    }
    
}
