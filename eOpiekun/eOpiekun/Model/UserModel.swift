//
//  UserModel.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 15/03/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import Foundation
import HandyJSON

class UserModel: HandyJSON {
    var firstName = ""
    var lastName = ""
    var userCity = ""
    var userAge = 0
    var friend = ""
    var userFriendModel :UserModel?
    var doctorId = ""
    var userId = ""
    var password = ""
    var contacts = [String]()
    
    required init() {}
}
