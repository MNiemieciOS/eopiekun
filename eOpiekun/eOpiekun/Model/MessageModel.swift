//
//  MessageModel.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 07/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import HandyJSON

class MessageModel: HandyJSON {
    var messageDetails = ""
    var messageId = ""
    var source = ""
    var timestamp = ""
    required init() {}
    
    init(withMessageDetails details: String) {
        messageDetails = details
        source = DataBaseManager.sharedInstnace.currentUser.userId
        timestamp = Date().description
        messageId = "\(DataBaseManager.sharedInstnace.currentUser.userId)_\(timestamp)"
        
    }
}
