//
//  AppDelegate.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 01/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        UINavigationBar.appearance().barTintColor = UIColor(red: 12/255.0, green: 180/255.0, blue: 255/255.0, alpha: 1.0)
        let fontSize = UIDevice.current.model.contains("iPrhone") == true ? 15 : 30
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: CGFloat(fontSize)), NSForegroundColorAttributeName: UIColor.white]
        UINavigationBar.appearance().tintColor = UIColor.white
        UIApplication.shared.statusBarStyle = .lightContent
        FIRApp.configure()
        return true
    }
}
