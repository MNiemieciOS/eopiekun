//
//  RoundedUIView.swift
//  eOpiekun
//
//  Created by Michal Niemiec on 06/02/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class RoundedUIView: UIView {

    override func layoutSubviews() {
        self.layer.cornerRadius = self.frame.size.width/2.0;
        self.layer.masksToBounds = true;
    }

}
