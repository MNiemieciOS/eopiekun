//
//  RoundedImageView.swift
//  Showcase
//
//  Created by Michal Niemiec on 01/12/2016.
//  Copyright © 2016 Hybris. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {
    override func layoutSubviews() {
        self.layer.cornerRadius = self.frame.size.width/2.0;
        self.layer.masksToBounds = true;
    }
}
